//
//  ViewController.swift
//  Target
//
//  Created by Volodymyr on 4/25/19.
//  Copyright © 2019 Volodymyr. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        drawBox(3)
    }
    // генерируем случайныйцвет
    func getRandomColor() -> UIColor{
        //Generate between 0 to 1
        let red:CGFloat = CGFloat(drand48())
        let green:CGFloat = CGFloat(drand48())
        let blue:CGFloat = CGFloat(drand48())
        
        return UIColor(red:red, green: green, blue: blue, alpha: 1.0)
    }
    
    func drawBox(_ quantityOfBox: Int) {
        var pointOfStartX: Double = 30
        var pointOfStartY: Double = 124
        var sizeOfTarget: Double = 260
        var radiusX = sizeOfTarget / 2
        var minuteOfAngle: Double = 0
        var newSizeOfTarget: Double = 0
        for j in 1...quantityOfBox {
            if j == 1 {
                newSizeOfTarget = sizeOfTarget
                let box = UIView()
                box.frame.size.width = CGFloat(sizeOfTarget)
                box.frame.size.height = CGFloat(sizeOfTarget)
                box.frame.origin.x = CGFloat(pointOfStartX)
                box.frame.origin.y = CGFloat(pointOfStartY)
                box.layer.cornerRadius = CGFloat(radiusX)
                box.backgroundColor = getRandomColor()
                view.addSubview(box)
            }
            if j == 2 {
                newSizeOfTarget = newSizeOfTarget / 100 * 80
                minuteOfAngle = (sizeOfTarget - newSizeOfTarget) / 2
                pointOfStartX += minuteOfAngle
                radiusX = newSizeOfTarget / 2
                pointOfStartY += minuteOfAngle
                let box = UIView()
                box.frame.size.width = CGFloat(newSizeOfTarget)
                box.frame.size.height = CGFloat(newSizeOfTarget)
                box.frame.origin.x = CGFloat(pointOfStartX)
                box.frame.origin.y = CGFloat(pointOfStartY)
                box.layer.cornerRadius = CGFloat(radiusX)
                box.backgroundColor = getRandomColor()
                view.addSubview(box)
            }
            if j == 3 {
                var secondSizeOfTarget = newSizeOfTarget / 100 * 65
                minuteOfAngle = (newSizeOfTarget - secondSizeOfTarget) / 2
                pointOfStartX += minuteOfAngle
                radiusX = secondSizeOfTarget / 2
                pointOfStartY += minuteOfAngle
                let box = UIView()
                box.frame.size.width = CGFloat(secondSizeOfTarget)
                box.frame.size.height = CGFloat(secondSizeOfTarget)
                box.frame.origin.x = CGFloat(pointOfStartX)
                box.frame.origin.y = CGFloat(pointOfStartY)
                box.layer.cornerRadius = CGFloat(radiusX)
                box.backgroundColor = getRandomColor()
                view.addSubview(box)
            }
        }
    }
}
